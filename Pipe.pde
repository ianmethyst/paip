class Pipe {
  ParticleSystem particleSystem;
  PVector position;
  int rotation;

  boolean active;

  int speed;

  int liquidTimeStart;
  int liquidTimeStop;

  Pipe(PVector position_, int rotation_) {
    position = position_;
    rotation = rotation_;

    resetParticleSystem();
  } 

  void update() {
    int currentTime = millis();
    if (active && currentTime > liquidTimeStart && currentTime < liquidTimeStop) {
      addLiquid();
    }

    if (currentTime > liquidTimeStop && particleSystem.dead()) {
      active = false;
    }
  }

  void addLiquid() {
    int n = (int) random(0, 2);

    PVector vel;

    switch (rotation) {
    case POINT_LEFT:
      vel = new PVector(-20, 0);
      break;
    case POINT_RIGHT:
      vel = new PVector(20, 0);
      break;
    case POINT_DOWN:
      vel = new PVector(0, -20);
      break;
    default:
      vel = new PVector(0, 0);
    }

    switch (speed) {
    case SPEED_SLOW:
      vel.mult(0.5);
      break;
    case SPEED_MEDIUM:
      vel.mult(1);
      break;
    case SPEED_FAST:
      vel.mult(2);
      break;
    default:
      vel.mult(1);
    }

    particleSystem.addParticles(n, vel);
  }

  void display() {
    particleSystem.run();

    main.push();
    main.imageMode(CENTER);
    main.translate(position.x, position.y);


    switch (rotation) {
    case POINT_RIGHT: 
      main.rotate(0);
      break;

    case POINT_DOWN: 
      main.rotate(PI / 2);
      break;

    case POINT_LEFT:
      main.rotate(PI);
      break;
    }

    if (active) {
      switch (speed) {
      case SPEED_SLOW:
        main.image(pipeSprites[1], PIPE_WIDTH / 2, 0, PIPE_WIDTH, PIPE_HEIGHT);
        break;
      case SPEED_MEDIUM:
        main.image(pipeSprites[2], PIPE_WIDTH / 2, 0, PIPE_WIDTH, PIPE_HEIGHT);
        break;
      case SPEED_FAST:
        main.image(pipeSprites[3], PIPE_WIDTH / 2, 0, PIPE_WIDTH, PIPE_HEIGHT);
        break;
      }
    } else {
      main.image(pipeSprites[0], PIPE_WIDTH / 2, 0, PIPE_WIDTH, PIPE_HEIGHT);
    }
    main.pop();
  }

  void setLiquidTime(int time) {
    // time in seconds
    liquidTimeStart = millis() + LIQUID_TIME_DELAY;
    liquidTimeStop = millis() + LIQUID_TIME_DELAY + time * 1000;
  }

  boolean isActive() {
    return active;
  }

  void setActive(boolean b) {
    active = b;
    speed = floor(random(0, 3));
  }

  void resetParticleSystem() {
    PVector pos;

    switch (rotation) {
    case POINT_RIGHT: 
      pos = new PVector(position.x + PIPE_WIDTH, position.y + PIPE_HEIGHT * 0.1);
      break;

    case POINT_DOWN: 
      pos = new PVector(position.x - PIPE_HEIGHT * 0.1, position.y + PIPE_WIDTH);
      break;

    case POINT_LEFT:
      pos = new PVector(position.x - PIPE_WIDTH, position.y);
      break;

    default:
      pos = new PVector(- PIPE_HEIGHT / 2, PIPE_WIDTH * 0.8);
    } 

    particleSystem = new ParticleSystem(pos);
  }
}
