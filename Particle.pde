// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2012
// Box2DProcessing example

// A Particle

class Particle {

  // We need to keep track of a Body
  Body body;

  PVector[] trail;

  PVector initialSpeed;

  // Constructor
  Particle(float x_, float y_, PVector initialSpeed_) {
    float x = x_;
    float y = y_;

    initialSpeed = initialSpeed_.copy();

    trail = new PVector[6];
    for (int i = 0; i < trail.length; i++) {
      trail[i] = new PVector(x, y);
    }

    // Add the box to the box2d world
    // Here's a little trick, let's make a tiny tiny radius
    // This way we have collisions, but they don't overwhelm the system
    makeBody(new Vec2(x, y), 0.2f);
  }

  // This function removes the particle from the box2d world
  void killBody() {
    box2d.destroyBody(body);
  }

  // Is the particle ready for deletion?
  boolean done() {
    // Let's find the screen position of the particle
    Vec2 pos = box2d.getBodyPixelCoord(body);
    Barrel b = game.getActiveBarrel();

    // Barrel check
    if (
      pos.x > b.getPosition().x - BARREL_WIDTH / 2&&
      pos.x < b.getPosition().x + BARREL_WIDTH / 2 &&
      pos.y > b.getPosition().y - BARREL_HEIGHT &&
      pos.y < b.getPosition().y + BARREL_HEIGHT
      ) {
      killBody();
      barrelSound.trigger();
      return true;
    }

    PVector mapped = mapPixelsToMain(round(pos.x), round(pos.y));
    
    color c = fg.get(int(pos.x), int(pos.y));

    // Foreground check
    if (fg.get(int(pos.x), int(pos.y)) == color(14, 34, 45, 255)) {
      killBody();
      game.setHealth(game.getHealth() - 1.25);
      splatterSound.trigger();
      return true;
    }

    // Pgrahpics bounds check
    if (pos.x > main.width || pos.x < 0 || pos.y > height || pos.y < 0 ) {
      killBody();
      game.setHealth(game.getHealth() - 1.25);
      splatterSound.trigger();
      return true;
    }

    return false;
  }

  // Drawing the box
  void display() {
    // We look at each body and get its screen position
    Vec2 pos = box2d.getBodyPixelCoord(body);

    // Keep track of a history of screen positions in an array
    for (int i = 0; i < trail.length-1; i++) {
      trail[i] = trail[i+1];
    }
    trail[trail.length-1] = new PVector(pos.x, pos.y);

    // Draw particle as a trail
    // bloomable
    main.beginShape();
    main.pushStyle();
    //noStroke();
    main.noFill();
    main.strokeWeight(4);
    main.stroke(NEON_LIQUID);  
    main.strokeCap(ROUND);
    main.strokeJoin(ROUND);

    for (int i = 0; i < trail.length; i++) {
      main.vertex(trail[i].x, trail[i].y);
      //ellipse(trail[i].x, trail[i].y, 20, 20);
    }
    main.popStyle();
    main.endShape();
  }

  // This function adds the rectangle to the box2d world
  void makeBody(Vec2 center, float r) {
    // Define and create the body
    BodyDef bd = new BodyDef();
    bd.type = BodyType.DYNAMIC;

    bd.position.set(box2d.coordPixelsToWorld(center));
    body = box2d.createBody(bd);

    // Give it some initial random velocity
    body.setLinearVelocity(new Vec2(initialSpeed.x + random(-1, 1), initialSpeed.y + random(-1, 1)));

    // Make the body's shape a circle
    CircleShape cs = new CircleShape();
    cs.m_radius = box2d.scalarPixelsToWorld(r);

    FixtureDef fd = new FixtureDef();
    fd.shape = cs;

    fd.density = 1;
    fd.friction = 0;  // Slippery when wet!
    fd.restitution = 0.5;

    // We could use this if we want to turn collisions off
    //cd.filter.groupIndex = -10;

    // Attach fixture to body
    body.createFixture(fd);
  }
}
