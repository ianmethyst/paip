class Barrel {

  PVector position;
  boolean active;

  Barrel(PVector position_) {
    position = position_;
  }

  void display() {
    main.pushStyle();
    main.imageMode(CENTER);
    main.pushMatrix();
    main.translate(position.x, position.y);
    
    if (active) {
      main.image(barrelSprites[1], 0, - BARREL_HEIGHT / 2, BARREL_WIDTH, BARREL_HEIGHT);
    } else {
      main.image(barrelSprites[0], 0, - BARREL_HEIGHT  / 2, BARREL_WIDTH, BARREL_HEIGHT);
    }

    main.popMatrix();
    main.popStyle();
  }

  void setActive(boolean b) {
    active = b;
  }

  boolean isActive() {
    return active;
  }

  PVector getPosition() {
    return position;
  }
}
