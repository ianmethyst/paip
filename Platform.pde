// The Nature of Code
// <http://www.shiffman.net/teaching/nature>
// Spring 2010
// Box2DProcessing example

// A fixed boundary class (now incorporates angle)

class Platform {

  // A boundary is a simple rectangle with x,y,width,and height
  float x;
  float y;
  float w;
  float h;

  // But we also have to make a body for box2d to know about it
  Body body;

  boolean rotatingRight, rotatingLeft;

  boolean clicked = false;

  Platform(float x_, float y_, float w_, float h_, float a) {
    x = x_;
    y = y_;
    w = w_;
    h = h_;

    // Define and create the body
    BodyDef bd = new BodyDef();
    bd.type = BodyType.KINEMATIC;
    bd.position.set(box2d.coordPixelsToWorld(x, y));
    bd.angle = a;

    body = box2d.createBody(bd);

    // Define a polygon (this is what we use for a rectangle)
    PolygonShape sd = new PolygonShape();
    float box2dW = box2d.scalarPixelsToWorld(w_/2);
    float box2dH = box2d.scalarPixelsToWorld(h_/2);
    sd.setAsBox(box2dW, box2dH);

    // Define a fixture
    FixtureDef fd = new FixtureDef();
    fd.shape = sd;
    // Parameters that affect physics
    fd.density = 1;
    fd.friction = 0.3;
    fd.restitution = 0.5;

    body.createFixture(fd);
  }

  void updateMouse() {
    if (clicked) {
      Vec2 mouse = box2d.coordPixelsToWorld(mouseX - (WIDTH - main.width) / 2, mouseY - (HEIGHT - main.height) / 2);
      body.setTransform(mouse, body.getAngle());
    } 

    if (rotatingLeft) {
      body.setTransform(body.getWorldCenter(), body.getAngle() - 0.03);
    } else if (rotatingRight) {
      body.setTransform(body.getWorldCenter(), body.getAngle() + 0.03);
    }
  }

  void updateCV(CV_Cursor c) {
    if (c.getBlobs() != null) {
      float x = c.getX();
      float y = c.getY();
      
      PVector from = cv.getMapFrom();
      PVector to = cv.getMapTo();
      
      Vec2 pos = box2d.coordPixelsToWorld(
        map(x, from.x, to.x, WIDTH
        , 0), 
        map(y, from.y, to.y, 100, HEIGHT - 100)
        );

      println("setting" + " - x: " + x + "(" + map(x, 0, 640, 0, WIDTH) + ") - y:" + y);

      body.setTransform(pos, c.getAngle());
      
    } else {
      Vec2 pos = box2d.coordPixelsToWorld(-1000, -1000);
      body.setTransform(pos, c.getAngle());
    }
  }

  // Draw the boundary, if it were at an angle we'd have to do something fancier
  void display() {
    Vec2 pos = box2d.getBodyPixelCoord(body);
    float a = body.getAngle();

    if (CV_ENABLED && debug) {
      main.pushStyle();
      main.fill(255, 0, 0);
      main.rectMode(CENTER);
      main.noStroke();

      main.pushMatrix();
      main.translate(pos.x, pos.y);
      main.rotate(-a);

      main.rect(0, 0, w, h);

      main.popMatrix();
      main.popStyle();
    } else if (!CV_ENABLED) {
      main.pushStyle();
      main.fill(FOREGROUND_COLOR);
      main.noStroke();
      main.imageMode(CENTER);

      main.pushMatrix();
      main.translate(pos.x, pos.y);
      main.rotate(-a);
      if (clicked) {
        main.image(platformSprites[1], 0, 0);
      } else {
        main.image(platformSprites[0], 0, 0);
      }
      main.popMatrix();
      main.popStyle();
    }
  }

  private boolean contains(float x, float y) {
    Vec2 worldPoint = box2d.coordPixelsToWorld(x, y);
    Fixture f = body.getFixtureList();
    boolean inside = f.testPoint(worldPoint);
    return inside;
  }

  boolean isClicked() {
    return clicked;
  }

  void handleMouse(boolean pressed) {
    if (pressed) {
      if (contains(mouseX - (WIDTH - main.width) / 2, mouseY  - (HEIGHT - main.height) / 2)) {
        clicked = true;
        platformOnSound.trigger();
      }
    } else {
      if (clicked) {
        clicked = false;
        rotatingLeft = rotatingRight = false;
        platformOffSound.trigger();
      }
    }
  }

  void handleKey(boolean pressed) {
    if (clicked) {
      switch (key) {
      case 'a':
      case 'A':
        rotatingLeft = pressed;
        break;

      case 's':
      case 'S':
        rotatingRight = pressed;
        break;
      }
    }
  }

  void setTransform(float x, float y, float angle) {
    body.setTransform(box2d.coordPixelsToWorld(x, y), angle);
  }

  void killBody() {
    box2d.destroyBody(body);
  }
  
  PVector getPosition() {
    PVector p = box2d.getBodyPixelCoordPVector(body);
    return p;
  }
}
