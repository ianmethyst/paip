class CV {
  // Blob min and max size;
  static final float minSize = 120;
  static final float maxSize = 350;
  static final int maxPieces = 3;

  PApplet parent;

  Capture cam;
  OpenCV opencv;
  PImage src;

  static final int maxBlobSize = 40;

  //PImage firstFrame;

  ArrayList<Contour> contours;
  CV_Cursor[] cursors;

  int maxColors = 3;
  int[] hues;
  int[] colors;
  int rangeWidth = 10;

  PImage[] outputs;
  int colorToChange = -1;

  int imageToDraw = CAMERA;

  PVector mapFrom, mapTo;

  boolean displayBlobs;

  CV(PApplet parent) {
    this.parent = parent;

    cam = new Capture(this.parent, 640, 480);
    opencv = new OpenCV(this.parent, cam.width, cam.height);

    mapFrom = new PVector(0, 0);
    mapTo = new PVector(cam.width, cam.height);

    cursors = new CV_Cursor[maxPieces];
    for (int i = 0; i < cursors.length; i++) {
      cursors[i] = new CV_Cursor(i);
    }

    colors = new int[maxColors];
    hues = new int[maxColors];

    outputs = new PImage[maxColors];

    cam.start();
  }

  void update() {
    if (cam.available() == true) {
      cam.read();
    }

    // <2> Load the new frame of our movie in to OpenCV
    opencv.loadImage(cam);

    // Tell OpenCV to use color information
    opencv.useColor();
    src = opencv.getSnapshot();

    // <3> Tell OpenCV to work in HSV color space.
    opencv.useColor(HSB);

    detectColors();

    for (CV_Cursor c : cursors) {
      c.update();
    }
  }

  void display(PGraphics p) {
    pushStyle();

    switch (imageToDraw) {
    case CAMERA:
      p.image(cam, 0, 0);

      p.stroke(0, 255, 255);
      p.line(mapFrom.x, 0, mapFrom.x, p.height);
      p.line(0, mapFrom.y, p.width, mapFrom.y);

      p.stroke(255, 0, 255);
      p.line(mapTo.x, 0, mapTo.x, p.height);
      p.line(0, mapTo.y, p.width, mapTo.y);

      break;
      //case CV:
      //  p.image(opencv.getOutput(), 0, 0);
      //  break;
    case CV:
      for (int i=0; i<outputs.length; i++) {
        if (outputs[i] != null) {
          p.fill(colors[i]);
          p.rect(0, i*src.height/3, p.width, src.height/3);
          p.image(outputs[i], p.width-src.width/3, i*src.height/3, src.width/3, src.height/3);
          p.noStroke();
        }
      }
      break;
    }

    for (CV_Cursor cursor : cursors) {
      if (displayBlobs) {
        cursor.display(p);
      }
    }

    fill(0, 255, 0);
    textAlign(LEFT, TOP);
    text(rangeWidth, 0, 0);

    for (int i = 0; i < hues.length; i++) {
      fill(colors[i]);
      noStroke();
      rect(0, i * 26 + 24, 24, 24);
    }

    popStyle();
  }

  void detectColors() {    
    for (int i=0; i < 3; i++) {
      opencv.loadImage(src);
      opencv.useColor(RGB);

      // <4> Copy the Hue channel of our image into 
      //     the gray channel, which we process.

      switch (i) {
      case 0: 
        opencv.setGray(opencv.getR().clone());
        break; 
      case 1: 
        opencv.setGray(opencv.getG().clone());
        break;
      case 2: 
        opencv.setGray(opencv.getB().clone());
        break;
      }

      //println("index " + i + " - hue to detect: " + hueToDetect);

      // <5> Filter the image based on the range of 
      //     hue values that match the object we want to track.
      //opencv.inRange(hueToDetect-rangeWidth/2, hueToDetect+rangeWidth/2);

      // TO DO:
      // Add here some image filtering to detect blobs better
      opencv.dilate();
      opencv.dilate();
      opencv.dilate();
      
      opencv.threshold(rangeWidth);

      // <6> Save the processed image for reference.
      outputs[i] = opencv.getSnapshot();
    }

    // <7> Find contours in our range image.
    //     Passing 'true' sorts them by descending area.
    for (int i = 0; i < outputs.length; i++) {
      if (outputs[i] == null) {
        cursors[i].assignBlobs(null);
      } else {
        cursors[i].assignBlobs(findBlobs(outputs[i]));
      }
    }
  }

  Rectangle[] findBlobs(PImage img, int expectedBlobs ) {
    opencv.loadImage(img);
    ArrayList<Contour> contours = opencv.findContours(false, true);

    Rectangle[] lights = new Rectangle[expectedBlobs];
    int assignedLights = 0;

    if (contours.size() < expectedBlobs) {
      return null;
    } else {
      for (int i = 0; assignedLights < expectedBlobs; i++) {
        if (i > contours.size() - 1) {
          return null;
        }

        Contour contour = contours.get(i);
        Rectangle r = contour.getBoundingBox();

        if (r.width > maxBlobSize || r.height > maxBlobSize) {
          continue;
        }

        lights[assignedLights] = r;
        assignedLights++;
      }

      return lights;
    }
  }

  Rectangle[] findBlobs(PImage src) {
    return findBlobs(src, 2);
  }

  void handleMousePressed() {
    if (colorToChange > -1) {
      if (debuggerSurface.isMouseOver()) {
        PVector mouse = debuggerSurface.getTransformedMouse();

        color c = debugger.get(int(mouse.x), int(mouse.y));

        int hue = int(map(hue(c), 0, 255, 0, 180));

        colors[colorToChange] = c;
        hues[colorToChange] = hue;
        cursors[colorToChange].setColor(c);

        println("color index " + (colorToChange) + ", value: " + hue);
      }
    }
  }

  void handleKeyPressed() {
    switch (key) {
    case 'i':
      mapFrom.set(mapFrom.x - 1, mapFrom.y);
      break;
    case 'I':
      mapFrom.set(mapFrom.x + 1, mapFrom.y);
      break;
    case 'j':
      mapFrom.set(mapFrom.x, mapFrom.y - 1);
      break;
    case 'J':
      mapFrom.set(mapFrom.x, mapFrom.y + 1);
      break;
    case 'o':
      mapTo.set(mapTo.x - 1, mapTo.y);
      break;
    case 'O':
      mapTo.set(mapTo.x + 1, mapTo.y);
      break;
    case 'k':
      mapTo.set(mapTo.x, mapTo.y - 1);
      break;
    case 'K':
      mapTo.set(mapTo.x, mapTo.y + 1);
      break;

    case 'b':
    case 'B':
      displayBlobs = !displayBlobs;
      break;

    case '1':
      colorToChange = 0;
      break;
    case '2': 
      colorToChange = 1;
      break;
    case '3':
      colorToChange = 2;
      break;
    case '+': 
      rangeWidth++;
      break;
    case '-':
      rangeWidth--;
      break;
    case '9':
      imageToDraw = CAMERA;
      break;
    case '0':
      imageToDraw = CV;
      break;

    case 'm':
    case 'M':
      saveCrop();
      break;

    case 'n':
    case 'N':
      loadCrop();
      break;
    }
  }

  void handleKeyReleased() {
    colorToChange = -1;
  }

  CV_Cursor[] getCursors() {
    return cursors;
  }

  PVector getMapFrom() {
    return mapFrom;
  }

  PVector getMapTo() {
    return mapTo;
  }

  void loadCrop() {
    JSONObject values;

    try {
      values = loadJSONObject("data/crop.json");
    } 
    catch(Exception e) {
      println("Crop data not found");
      return;
    }

    float fromX = values.getFloat("fromX");
    float fromY = values.getFloat("fromY");
    float toX = values.getFloat("toX");
    float toY = values.getFloat("toY");

    mapFrom.set(fromX, fromY);
    mapTo.set(toX, toY);
  }

  void saveCrop() {
    JSONObject values = new JSONObject();

    values.setFloat("fromX", mapFrom.x);
    values.setFloat("fromY", mapFrom.y);
    values.setFloat("toX", mapTo.x);
    values.setFloat("toY", mapTo.y);

    saveJSONObject(values, "data/crop.json");
  }
}
