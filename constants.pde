// Game state
final int MENU = 0;
final int GAME = 1;
final int RESULTS = 2;

final int SPEED_SLOW = 0;
final int SPEED_MEDIUM = 1;
final int SPEED_FAST = 2;

final int POINT_RIGHT = 0;
final int POINT_DOWN = 1;
final int POINT_LEFT = 2;

final int PIPE_WIDTH = 80;
final int PIPE_HEIGHT = 48;
final color PIPE_LIGHT_ON = #ff0000;
final color PIPE_LIGHT_OFF = #4d0000;

final int BARREL_WIDTH = 80;
final int BARREL_HEIGHT = 108;
final color BARREL_LIGHT_ON = #00ff00;
final color BARREL_LIGHT_OFF = #004d00;

final color LIQUID_COLOR = #b0ff94;
final color EMPTY_LIFE_BAR = #677746;

final int LIQUID_TIME_DELAY = 2000;

final int MAX_HEALTH = 200;
final color ORANGE_COLOR = #ffa42a;

final color FOREGROUND_COLOR = #071117;

final color NEON_LIQUID = #4AF317;

final int CAMERA = 9;
final int CV = 0;
