import shiffman.box2d.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;

import gab.opencv.*;
import processing.video.*;
import java.awt.Rectangle;

import ch.bildspur.postfx.builder.*;
import ch.bildspur.postfx.pass.*;
import ch.bildspur.postfx.*;

import ddf.minim.*;
import ddf.minim.effects.*;

import ianmethyst.keystone.*;

Box2DProcessing box2d;
//PostFX fx;
Minim minim;

Game game;
CV cv;
PGraphics main, debugger;
//PGraphics bloom;

PImage[] pipeSprites;
PImage[] barrelSprites;
PImage[] platformSprites;
PImage bg, fg;
PFont normalFont, fluidFont;
AudioPlayer menuMusic, gameMusic, liquidFlowSound;
AudioSample alertSound, barrelSound, platformOnSound, platformOffSound, splatterSound;

Keystone ks;
CornerPinSurface surface, debuggerSurface;

final int WIDTH = 1024;
final int HEIGHT = 768;

boolean debug = true;
final boolean CV_ENABLED = true;

void settings() {
  //size(WIDTH, HEIGHT, P3D);
  fullScreen(P3D, SPAN);
  smooth(8);
}

void setup() {
  frame.setTitle("floating");

  if (WIDTH > HEIGHT) {
    main = createGraphics(round(HEIGHT * 1.3333333), HEIGHT, P3D);
    //bloom = createGraphics(round(HEIGHT * 1.3333333), HEIGHT, P3D);
  } else {
    main = createGraphics(WIDTH, round(WIDTH * 0.75), P3D);
    //bloom = createGraphics(round(HEIGHT * 1.3333333), HEIGHT, P3D);
  }

  debugger = createGraphics(640, 480, P3D);

  main.smooth(8);

  box2d = new Box2DProcessing(this);
  box2d.createWorld();
  box2d.setGravity(0, -20);

  //fx = new PostFX(this);  
  minim = new Minim(this);
  ks = new Keystone(this);
  surface = ks.createCornerPinSurface(WIDTH, HEIGHT, 20);
  debuggerSurface = ks.createCornerPinSurface(640, 480, 20);
  surface.setSelected(true);
  debuggerSurface.setSelected(true);

  loadResources();

  game = new Game();

  if (CV_ENABLED) {
    cv = new CV(this);
  }
}

void draw() {
  if (!ks.isCalibrating()) {
    background(0);
  } else { 
    background(255, 0, 255);
  }

  game.update();

  main.beginDraw();
  //bloom.beginDraw();

  //bloom.clear();
  main.clear();
  game.display();
  //bloom.endDraw();
  main.endDraw();

  //pushStyle();
  //imageMode(CORNER);
  //blendMode(BLEND);
  //translate(0, 0);.
  //image(main, 0, 0);

  //blendMode(SCREEN);
  //image(bloom, 0, 0);

  //fx.render(bloom).bloom(0.5, 20, 20).compose();

  //popStyle();

  //blendMode(SCREEN);
  //fx.render(bloom)
  //  .bloom(0.5, 20, 20)
  //  .compose();

  surface.render(main);

  if (debug) {
    if (CV_ENABLED) {
      debugger.beginDraw();
      debugger.clear();
      cv.display(debugger);
      debugger.endDraw();
    }

    debuggerSurface.render(debugger);
  }

  if (ks.isCalibrating()) {
    fill(0, 255, 255);
    ellipse(mouseX, mouseY, 20, 20);
  }
}

void mousePressed() {
  if (CV_ENABLED) {
    cv.handleMousePressed();
  }
  game.handleMouse(true);
}

void mouseReleased() {
  game.handleMouse(false);
}

void keyPressed() {
  if (CV_ENABLED) {
    cv.handleKeyPressed();
  }

  game.handleKey(true);

  switch (key) {
  case 'd':
  case 'D':
    debug = !debug;
    break;

  case 'c':
  case 'C':
    ks.toggleCalibration();
    break;

  case 'l':
  case 'L':
    // loads the saved layout
    ks.load();
    break;

  case 'v':
  case 'V':
    // saves the layout
    ks.save();
    break;
  }
}

void keyReleased() {
  if (CV_ENABLED) {
    cv.handleKeyReleased();
  }

  game.handleKey(false);
}

void loadResources() {
  pipeSprites = new PImage[4];
  pipeSprites[0] = loadImage("data/img/pipe/off.png");
  pipeSprites[1] = loadImage("data/img/pipe/on_1.png");
  pipeSprites[2] = loadImage("data/img/pipe/on_2.png");
  pipeSprites[3] = loadImage("data/img/pipe/on_3.png");

  barrelSprites = new PImage[2];
  barrelSprites[0] = loadImage("data/img/barrel/off.png");
  barrelSprites[1] = loadImage("data/img/barrel/on.png");

  platformSprites = new PImage[2];
  platformSprites[0] = loadImage("data/img/platform/off.png");
  platformSprites[1] = loadImage("data/img/platform/on.png");

  bg = loadImage("data/img/bg.png");
  fg = loadImage("data/img/fg.png");

  normalFont = createFont("data/font/regular.otf", main.width / 10);
  fluidFont = createFont("data/font/fluid.otf", main.width / 5);

  gameMusic = minim.loadFile("music/game.mp3", 2048);
  menuMusic = minim.loadFile("music/menu.mp3", 2048);
  liquidFlowSound = minim.loadFile("sound/liquid_flow.mp3", 2048);

  alertSound = minim.loadSample("sound/alert.wav", 512);
  barrelSound = minim.loadSample("sound/barrel.wav", 512);
  platformOffSound = minim.loadSample("sound/platform_off.wav", 512);
  platformOnSound = minim.loadSample("sound/platform_on.wav", 512);
  splatterSound = minim.loadSample("sound/splatter.wav", 512);
}

PVector mapPixelsToMain (int x, int y) {
  PVector v = new PVector(
    map(x, 0, 1024, 0, main.width), 
    map(y, 0, 768, 0, main.height)
    );

  return v;
}
