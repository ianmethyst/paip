#!/bin/bash
set -Eeuo pipefail

# This script installs Box2D-for-Processing in Processing's sketchbook

sketchbook=$(cat $HOME/.processing/preferences.txt | grep sketchbook | cut -d "=" -f 2)

echo "Downloading Box2D-for-Processing into $sketchbook/libraries"

wget -P /tmp http://www.shiffman.net/p5/libraries/box2d_processing/box2d_processing.zip 

unzip /tmp/box2d_processing.zip -x "__MACOSX/*" -d $sketchbook/libraries
