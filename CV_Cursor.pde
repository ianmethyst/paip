class CV_Cursor {
  PVector position;
  float rotation;

  int id;

  boolean ease;
  final static float easing = 0.05;

  Rectangle[] blobs;

  color colour;

  CV_Cursor(int id) {
    this.id = id;
    position = new PVector();
  }

  void update() {
    if (blobs != null) {
      position.set(lerp(blobs[0].x, blobs[1].x, 0.5), lerp(blobs[1].y, blobs[1].y, 0.5));
      rotation = atan2(blobs[1].y - blobs[0].y, blobs[1].x - blobs[0].x);
    }
  }

  void display(PGraphics p) {
    if (blobs != null) {
      p.pushStyle();
      p.fill(colour, 50);
      p.rect(blobs[0].x, blobs[0].y, blobs[0].width, blobs[0].height);
      p.rect(blobs[1].x, blobs[1].y, blobs[1].width, blobs[1].height);

      p.noStroke();
      p.fill(colour, 75);
      p.ellipse(blobs[0].x + blobs[0].width / 2, blobs[0].y + blobs[0].height / 2, 10, 10);
      p.ellipse(blobs[1].x + blobs[1].width / 2, blobs[1].y + blobs[1].height / 2, 10, 10);

      //p.fill(255);
      //p.text(id + "\n" + boundingBoxSize +"\n" + rotation, boundingBox.x + boundingBox.width / 2, boundingBox.y  + boundingBox.height / 2);
      p.popStyle();
    }
  }

  void assignBlobs(Rectangle[] blobs) {
      this.blobs = blobs;
  }

  Rectangle[] getBlobs() {
    return blobs;
  }

  float getX() {
    return position.x;
  }

  float getY() {
    return position.y;
  }

  float getAngle() {
    return rotation;
  }

  color getColor() {
    return colour;
  }

  void setColor(color c) {
    colour = c;
  }
}
