class Game {
  int currentState;

  static final String introText = "_Move las piezas con el mouse_\n_Rotalas con A y S_\n_No dejes que el liquido toque el suelo_";
  static final String introTextCV = "_Utilizá las piezas para dirigir el liquido\n hacia el barril iluminado_";

  Pipe[] pipes;
  Barrel[] barrels;
  Platform[] platforms;

  float health;
  int gameStart;
  int record;

  int records[];
  int lastScore;

  int startTime;

  Pipe activePipe;
  Barrel activeBarrel;

  public Game() {
    currentState = MENU;

    pipes = new Pipe[3];
    platforms = new Platform[3];
    barrels = new Barrel[3];

    pipes[0] = new Pipe(mapPixelsToMain(120, 145), POINT_RIGHT);
    pipes[1] = new Pipe(mapPixelsToMain(518, 60), POINT_DOWN);
    pipes[2] = new Pipe(mapPixelsToMain(828, 285), POINT_LEFT);

    barrels[0] = new Barrel(new PVector(128, 602));
    barrels[1] = new Barrel(new PVector(349, 728));
    barrels[2] = new Barrel(new PVector(795, 626));

    resetGame();
  }

  public void resetGame() {
    health = MAX_HEALTH;
    gameStart = -1;

    activePipe = null;
    activeBarrel = null;

    for (int i = 0; i < platforms.length; i++) {
      if (platforms[i] != null) {
        platforms[i].killBody();
      }
    }

    for (int i = 0; i < pipes.length; i++) {
      pipes[i].setActive(false);
      pipes[i].resetParticleSystem();
    }

    for (int i = 0; i < barrels.length; i++) {
      barrels[i].setActive(false);
    }

    platforms[0] = new Platform(main.width / 2, main.height * 0.4, 180, 24, random(-0.5, 0.5));
    platforms[1] = new Platform(main.width / 2, main.height * 0.5, 180, 24, random(-0.5, 0.5));
    platforms[2] = new Platform(main.width / 2, main.height * 0.6, 180, 24, random(-0.5, 0.5));
  }

  public void update() {
    if (CV_ENABLED) {
      cv.update();

      CV_Cursor[] cursors = cv.getCursors();

      for (int i = 0; i < cursors.length; i++) {
        platforms[i].updateCV(cursors[i]);
      }
    }

    switch (currentState) {
    case MENU:
      if (!menuMusic.isPlaying()) {
        gameMusic.pause();
        menuMusic.rewind();
        menuMusic.loop();
      }

      if (CV_ENABLED) {
        boolean starting = false;
        for (Platform p : platforms) {
          if (starting) continue;
          PVector pos = p.getPosition();
          if (dist(pos.x, pos.y, main.width / 2, main.height * 0.75) < 50) {
            starting = true;
          }
        }

        if (starting) {
          if (startTime < 0) {
            startTime = millis() + 1000;
          }

          if (millis() > startTime) {
            currentState = GAME;
            startTime = -1;
          }
        } else {
          startTime = -1;
        }
      }

      break;

    case RESULTS:
      if (CV_ENABLED) {
        boolean quiting = true;

        for (Platform p : platforms) {
          PVector pos = p.getPosition();

          if (pos.x > 0 && pos.y > 0 && pos.x < main.width && pos.y < main.height) {
            quiting = false;
          }
        }

        if (quiting) {
          if (startTime < 0) {
            startTime = millis() + 1000;
          }

          if (millis() > startTime) {
            currentState = MENU;
            resetGame();
            startTime = -1;
          }
        } else {
          startTime = -1;
        }
      }


      break;

    case GAME:
      if (!gameMusic.isPlaying()) {
        menuMusic.pause();
        gameMusic.rewind();
        gameMusic.loop();
      }

      box2d.step();
      gameLogic();

      for (Pipe pipe : pipes) {
        pipe.update();
      }

      if (!CV_ENABLED) {
        for (Platform platform : platforms) {
          platform.updateMouse();
        }
      }
      break;
    }
  }

  public void display() {
    switch (currentState) {
    case MENU:
      displayMenuScreen();
      break;

    case GAME:
      main.image(bg, 0, 0, main.width, main.height);

      for (Pipe pipe : pipes) {
        pipe.display();
      }

      for (Barrel barrel : barrels) {
        barrel.display();
      }

      main.image(fg, 0, 0, main.width, main.height);

      for (Platform platform : platforms) {
        platform.display();
      }

      displayHUD();
      break;


    case RESULTS:
      displayResultsScreen();
      break;
    }
  }

  public void displayMenuScreen() {
    main.pushStyle();
    main.image(bg, 0, 0, main.width, main.height);
    main.image(fg, 0, 0, main.width, main.height);
    main.fill(0, 0, 0, 125);
    main.rect(0, 0, main.width, main.height);

    main.fill(FOREGROUND_COLOR);
    main.rectMode(CENTER);
    main.noStroke();
    main.rect(main.width / 2, main.height * 0.5, main.width * 0.42, main.height * 0.38, 25);

    //bloomable
    main.textAlign(CENTER, CENTER);
    main.textFont(fluidFont);
    main.textSize(main.width / 5);
    main.fill(NEON_LIQUID);
    main.text("PAIP", main.width / 2, 200);

    main.fill(LIQUID_COLOR);
    main.textAlign(CENTER, CENTER);
    main.textFont(normalFont);
    main.textSize(main.width / 32);
    main.textLeading(main.width / 32);
    if (!CV_ENABLED) {
      main.text(introText, main.width / 2, 380);
    } else {
      main.text(introTextCV, main.width / 2, 380);
    }
    main.textSize(main.width / 22);
    main.fill(ORANGE_COLOR);
    if (!CV_ENABLED) {
      main.text("Pulsa espacio para comenzar", main.width / 2, 480);
    } else {
      main.text("Poné una pieza acá para comenzar", main.width / 2, 480);

      main.fill(FOREGROUND_COLOR);
      main.rectMode(CENTER);
      main.noStroke();
      main.rect(main.width / 2, main.height * 0.75, 220, 35, 25);
    }
    main.popStyle();
  }

  public void displayResultsScreen() {
    main.pushStyle();
    main.image(bg, 0, 0, main.width, main.height);
    main.image(fg, 0, 0, main.width, main.height);
    main.fill(0, 0, 0, 125);
    main.rect(0, 0, main.width, main.height);

    main.fill(FOREGROUND_COLOR);
    main.rectMode(CENTER);
    main.noStroke();
    main.rect(main.width / 2, main.height * 0.502, main.width * 0.28, main.height * 0.59, 25);

    //bloomable
    if (!CV_ENABLED) {
      main.textAlign(CENTER, CENTER);
      main.textFont(fluidFont);
      main.textSize(main.width / 10);
      main.fill(NEON_LIQUID);
      main.text("PAIP", main.width / 2, 80);
    }

    main.textAlign(CENTER, CENTER);
    main.fill(LIQUID_COLOR);
    main.textFont(normalFont);
    main.textSize(main.width / 28);
    main.text("Mejores tiempos:", main.width / 2, 200 );

    for (int i = 0; i < records.length; i++) {
      if (records[i] != lastScore) {
        main.textSize(main.width / 16);
        main.fill(LIQUID_COLOR);
        main.text(records[i], main.width / 2, main.height / 3 + (i * main.width / 12));
      } else {
        main.textSize(main.width / 13);
        main.fill(ORANGE_COLOR);
        main.text(records[i], main.width / 2, main.height / 3 + (i * main.width / 12));
      }
    }

    main.fill(ORANGE_COLOR);
    main.textSize(main.width / 28);
    if (CV_ENABLED) {
      main.text("Sacá todas las piezas", main.width / 2, 580);
    } else {
      main.text("Pulsa cualquier tecla", main.width / 2, 620);
    }
    main.popStyle();
  }

  public void displayHUD() {
    main.pushStyle();

    // Lifebar
    main.rectMode(CORNER);
    main.noStroke();

    main.fill(EMPTY_LIFE_BAR);
    main.rect(20, 10, main.width - 40, 15, 20);
    main.fill(LIQUID_COLOR);
    main.rect(20, 10, constrain(map(health, 0, MAX_HEALTH, 0, main.width - 40), 0, main.width - 40), 15, 20);

    // time elapsed
    PVector timePos = mapPixelsToMain(860, 90);

    main.textAlign(CENTER, CENTER);
    main.fill(LIQUID_COLOR);
    main.textFont(normalFont);
    String time = Integer.toString((millis() - gameStart) / 1000);
    main.text(time, timePos.x, timePos.y);
    main.popStyle();
  }

  public void gameLogic() {
    if (gameStart == -1) {
      gameStart = millis();
    }

    health += 0.01;

    if (activePipe == null) {
      activePipe = pipes[floor(random(0, pipes.length))];
      activePipe.setActive(true);
      alertSound.trigger();

      activePipe.setLiquidTime(6);

      activeBarrel = barrels[floor(random(0, barrels.length))];
      activeBarrel.setActive(true);
    }

    if (!activePipe.isActive()) {
      activePipe = null;
      activeBarrel.setActive(false);
      activeBarrel = null;
    }

    if (health <= 0) {
      handleResults();
      currentState = RESULTS;
      record = (millis() - gameStart) / 1000;
    }
  }

  public void handleResults() { 
    JSONArray hiScores = loadJSONArray(sketchPath() + "/hi_scores.json");

    int score = (millis() - gameStart) / 1000;

    int place = -1;

    for (int i = hiScores.size() - 1; i >= 0; i--) {
      int currentHi = hiScores.getInt(i);

      if (score > currentHi) {
        place = i;
        continue;
      } else { 
        break;
      }
    }

    if (place == -1) {
      place = hiScores.size() -1;
    }

    int[] newHiScores = new int[hiScores.size()];
    JSONArray save = new JSONArray();

    for (int i = 0; i < hiScores.size(); i++) {
      if (i < place) {
        newHiScores[i] = hiScores.getInt(i);
        save.setInt(i, newHiScores[i]);
      } else if (i > place) {
        newHiScores[i] = hiScores.getInt(i - 1);
        save.setInt(i, newHiScores[i]);
      }
    }

    newHiScores[place] = score;
    save.setInt(place, newHiScores[place]);

    records = newHiScores;
    lastScore = score;
    saveJSONArray(save, sketchPath() + "/hi_scores.json");
  }


  public void handleMouse(boolean pressed) {
    if (pressed) {
      for (Platform platform : platforms) {
        for (int i = platforms.length - 1; i > 0; i--) {
          if (platforms[i].isClicked()) {
            return;
          }
          platform.handleMouse(true);
        }
      }
    } else { 
      for (Platform platform : platforms) {
        platform.handleMouse(false);
      }
    }
  }

  public void handleKey(boolean pressed) {
    if (pressed) {
      switch (currentState) {
      case GAME: 
        if (key == 'K') {
          health = -1;
        }

        for (Platform platform : platforms) {
          platform.handleKey(true);
        }
        break;
      }
    } else { 
      switch (currentState) {
      case MENU:
        if (key == ' ') {
          currentState = GAME;
        }
        break;
      case GAME: 
        for (Platform platform : platforms) {
          platform.handleKey(false);
        }
        break;
      case RESULTS:
        if (key == ' ') {
          resetGame();
          currentState = MENU;
        }
        break;
      }
    }
  }

  float getHealth() {
    return health;
  }

  void setHealth(float h) {
    health = h;
  }

  Barrel getActiveBarrel() {
    return activeBarrel;
  }
}
